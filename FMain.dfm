object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'Undo Sample'
  ClientHeight = 378
  ClientWidth = 421
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 19
  object edtText: TEdit
    Left = 32
    Top = 32
    Width = 273
    Height = 27
    TabOrder = 0
  end
  object btnAdd: TButton
    Left = 320
    Top = 33
    Width = 75
    Height = 25
    Action = actAdd
    TabOrder = 1
  end
  object lbxText: TListBox
    Left = 32
    Top = 80
    Width = 273
    Height = 241
    ItemHeight = 19
    TabOrder = 3
  end
  object btnUp: TButton
    Left = 32
    Top = 337
    Width = 75
    Height = 25
    Action = actUp
    TabOrder = 4
  end
  object btnDown: TButton
    Left = 230
    Top = 337
    Width = 75
    Height = 25
    Action = actDown
    TabOrder = 6
  end
  object btnUndo: TButton
    Left = 320
    Top = 80
    Width = 75
    Height = 25
    Action = actUndo
    TabOrder = 2
  end
  object btnDelete: TButton
    Left = 128
    Top = 337
    Width = 75
    Height = 25
    Action = actDelete
    TabOrder = 5
  end
  object actListUI: TActionList
    OnUpdate = actListUIUpdate
    Left = 344
    Top = 168
    object actAdd: TAction
      Caption = 'Add'
      OnExecute = actAddExecute
    end
    object actUp: TAction
      Caption = 'Up'
      OnExecute = actUpExecute
    end
    object actDown: TAction
      Caption = 'Down'
      OnExecute = actDownExecute
    end
    object actUndo: TAction
      Caption = 'Undo'
      OnExecute = actUndoExecute
    end
    object actDelete: TAction
      Caption = 'Delete'
      OnExecute = actDeleteExecute
    end
  end
end
