program Undo;

uses
  Vcl.Forms,
  FMain in 'FMain.pas' {frmMain},
  UUndoActions in 'UUndoActions.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
