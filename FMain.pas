unit FMain;

interface

uses
  System.Actions,
  System.Classes,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.ActnList,
  Vcl.StdCtrls,
  UUndoActions;

type
  TfrmMain = class(TForm)
    edtText: TEdit;
    btnAdd: TButton;
    actListUI: TActionList;
    lbxText: TListBox;
    btnUp: TButton;
    btnDown: TButton;
    actAdd: TAction;
    actUp: TAction;
    actDown: TAction;
    actUndo: TAction;
    btnUndo: TButton;
    btnDelete: TButton;
    actDelete: TAction;
    procedure actAddExecute(Sender: TObject);
    procedure actUpExecute(Sender: TObject);
    procedure actListUIUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure actDownExecute(Sender: TObject);
    procedure actUndoExecute(Sender: TObject);
    procedure actDeleteExecute(Sender: TObject);
  private
    FUndoStack: TUndoStack;
    procedure AddText;
    procedure DeleteText;
    procedure Restore(const Text: string;
                      const Index: Integer);
    procedure Remove(const Index: Integer);
    procedure MoveDown(const Index: Integer;
                       const RecordAction: Boolean = True);
    procedure MoveUp(const Index: Integer;
                     const RecordAction: Boolean = True);
    procedure Undo;
    { Private declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  System.SysUtils;

{$R *.dfm}

procedure TfrmMain.actAddExecute(Sender: TObject);
begin
  AddText;
end;

procedure TfrmMain.actDeleteExecute(Sender: TObject);
begin
  DeleteText;
end;

procedure TfrmMain.actDownExecute(Sender: TObject);
begin
  MoveDown(lbxText.ItemIndex);
end;

procedure TfrmMain.MoveDown(const Index: Integer;
                            const RecordAction: Boolean);
var
  NewIndex: Integer;
  Text: string;
  Action: TListAction;
begin
  Text := lbxText.Items[Index];
  lbxText.Items.Delete(Index);
  NewIndex := Index+1;
  lbxText.Items.Insert(NewIndex, Text);
  lbxText.ItemIndex := NewIndex;
  if RecordAction then
  begin
    Action := TListAction.Create(Text, NewIndex, loDown);
    FUndoStack.Push(Action);
  end;
end;

procedure TfrmMain.MoveUp(const Index: Integer;
                          const RecordAction: Boolean);
var
  NewIndex: Integer;
  Text: string;
  Action: TListAction;
begin
  Text := lbxText.Items[Index];
  lbxText.Items.Delete(Index);
  NewIndex := Index-1;
  lbxText.Items.Insert(NewIndex, Text);
  lbxText.ItemIndex := NewIndex;
  if RecordAction then
  begin
    Action := TListAction.Create(Text, NewIndex, loUp);
    FUndoStack.Push(Action);
  end;
end;

procedure TfrmMain.Remove(const Index: Integer);
begin
  lbxText.Items.Delete(Index);
end;

procedure TfrmMain.Restore(const Text: string; const Index: Integer);
begin
  lbxText.Items.Insert(Index, Text);
  lbxText.ItemIndex := Index;
end;

procedure TfrmMain.Undo;
var
  Action: TListAction;
begin
  Action := FUndoStack.Extract;
  try
    case Action.ListOperation of
      loAdd: Remove(Action.Index);
      loDelete: Restore(Action.Text, Action.Index);
      loUp: MoveDown(Action.Index, False);
      loDown: MoveUp(Action.Index, False);
    end;
  finally
    Action.Free;
  end;
end;

procedure TfrmMain.AddText;
var
  Index: Integer;
  Action: TListAction;
begin
  Index := lbxText.Items.Add(edtText.Text);
  lbxText.ItemIndex := Index;
  Action := TListAction.Create(edtText.Text, Index, loAdd);
  FUndoStack.Push(Action);
  edtText.Text := EmptyStr;
  edtText.SetFocus;
end;

constructor TfrmMain.Create(AOwner: TComponent);
begin
  inherited;
  FUndoStack := TUndoStack.Create;
end;

procedure TfrmMain.DeleteText;
var
  Text: string;
  Index: Integer;
  Action: TListAction;
begin
  Text := lbxText.Items[lbxText.ItemIndex];
  Index := lbxText.ItemIndex;
  Action := TListAction.Create(Text, Index, loDelete);
  FUndoStack.Push(Action);
  lbxText.Items.Delete(lbxText.ItemIndex);
end;

destructor TfrmMain.Destroy;
begin
  FUndoStack.Free;
  inherited;
end;

procedure TfrmMain.actListUIUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  actAdd.Enabled := (edtText.Text <> EmptyStr);
  actDelete.Enabled := (lbxText.Count > 0) and (lbxText.ItemIndex >= 0);
  actUp.Enabled := (lbxText.Count > 0) and (lbxText.ItemIndex > 0);
  actDown.Enabled := (lbxText.Count > 0) and (lbxText.ItemIndex < (lbxText.Count-1)) and (lbxText.ItemIndex >= 0);
  actUndo.Enabled := (FUndoStack.Count > 0);
  Handled := True;
end;

procedure TfrmMain.actUndoExecute(Sender: TObject);
begin
  Undo;
end;

procedure TfrmMain.actUpExecute(Sender: TObject);
begin
  MoveUp(lbxText.ItemIndex);
end;

end.
