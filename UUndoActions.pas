unit UUndoActions;

interface

uses
  System.Generics.Collections;

type
  TListOperation = (loAdd, loDelete, loUp, loDown);

  TListAction = class(TObject)
  private
    FText: string;
    FIndex: Integer;
    FListOperation: TListOperation;
  public
     constructor Create(const Text: string;
                        const Index: Integer;
                        const ListOperation: TListOperation);
     property Text: string read FText;
     property Index: Integer read FIndex;
     property ListOperation: TListOperation read FListOperation;
  end;

  TUndoStack = class(TObjectStack<TListAction>)
  end;

implementation

{ TAction }

constructor TListAction.Create(const Text: string;
                               const Index: Integer;
                               const ListOperation: TListOperation);
begin
  FText := Text;
  FIndex := Index;
  FListOperation := ListOperation;
end;

end.
